/*! uNgGrid 0.0.0
 *  Copyright (c) 2015 Dominik Marczuk
 *  Licence: BSD-2-Clause: http://opensource.org/licenses/BSD-2-Clause
 */
angular.module("mingos.uNgGrid", []);


angular.module("mingos.uNgGrid").directive("uNgGrid", function() {
	return {
		restrict: "A",
		scope: {
			service: "=uNgGrid"
		},
		controller: function($scope) {
			this.service = $scope.service;
		}
	};
});

angular.module("mingos.uNgGrid").directive("uNgGridSorter", function(
	uNgGridSorter
) {
	return {
		restrict: "AE",
		replace: true,
		require: "^uNgGrid",
		template:
			"<span class=\"u-ng-grid-sorter\" ng-click=\"sort()\">" +
				"<span class=\"{{ sortingClass }}\"></span>" +
			"</span>",
		scope: {
			field: "@"
		},
		link: function(scope, element, attributes, uNgGrid) {
			var type = attributes.type || "default";
			var classes = {
				asc: attributes.classAsc || uNgGridSorter.get(type, "asc"),
				desc: attributes.classDesc || uNgGridSorter.get(type, "desc"),
				none: attributes.classNone || uNgGridSorter.get(type, "none")
			};
			scope.sortingClass = classes.none;
			scope.service = uNgGrid.service;

			scope.$watchCollection("service.params", function() {
				if (scope.field === scope.service.params.orderfield) {
					if (-1 === scope.service.params.orderdirection) {
						scope.sortingClass = classes.desc;
					} else {
						scope.sortingClass = classes.asc;
					}
				} else {
					scope.sortingClass = classes.none;
				}
			});

			scope.sort = function() {
				if (scope.field === scope.service.params.orderfield) {
					scope.service.params.orderdirection = scope.service.params.orderdirection ?
					scope.service.params.orderdirection * -1 :
						1;
				} else {
					scope.service.params.orderdirection = 1;
				}

				scope.service.params.orderfield = scope.field;
			};
		}
	};
});


angular.module("mingos.uNgGrid").provider("uNgGrid", function() {
	/**
	 * Default server response callback
	 * @param {uNgGrid}  service
	 * @param {Array}    data
	 * @param {Function} headers
	 */
	var onResponse = function(service, data, headers) {
		service.data = data;
		service.total = parseInt(headers("Total-Items"), 10);
	};

	/**
	 * Default callback launched when the limit is updated
	 * @param {uNgGrid} service
	 */
	var onUpdateLimit = function(service) {
		service.params.offset = 0;
	};

	/**
	 * Default parametres transformation for sending along with the request
	 * @param   {uNgGrid} service
	 * @param   {Object}  params
	 * @returns {Object}
	 */
	var transformParams = function(service, params) {
		return params;
	};

	/**
	 * Set the default onResponse callback
	 * @param   {Function} cbk
	 * @returns {self}
	 */
	this.setOnResponse = function(cbk) {
		onResponse = cbk;
		return this;
	};

	/**
	 * Set the default onUpdateLimit callback
	 * @param   {Function} cbk
	 * @returns {self}
	 */
	this.setOnUpdateLimit = function(cbk) {
		onUpdateLimit = cbk;
		return this;
	};

	/**
	 * Set a custom parametre transformation function
	 * @param   {Function} cbk
	 * @returns {self}
	 */
	this.setTransformParams = function(cbk) {
		transformParams = cbk;
		return this;
	};

	/**
	 * Factory
	 * @returns {Function}
	 */
	this.$get = ["$rootScope", function($rootScope) {
		var uNgGrid = function(resource, pagination) {
			var me = this;

			/**
			 * A resource class (not instance)
			 */
			this.resource = resource;

			/**
			 * Parametres that will be sent with each request
			 */
			this.params = {
				limit: 10,
				offset: 0
			};

			/**
			 * Currently available data
			 */
			this.data = [];

			/**
			 * Total number of items that would match the current query without offset + limit
			 * @type {Number}
			 */
			this.total = 0;

			/**
			 * Flag indicating whether data is currently being loaded
			 * @type {Boolean}
			 */
			this.loading = false;

			/**
			 * Grid's pagination service
			 * @type {uNgGridPagination}
			 */
			this.pagination = pagination;
			if (this.pagination) {
				this.pagination.grid = this;
			}

			/**
			 * Callback used after the server responds with some data
			 * @param {Array}    data
			 * @param {Function} headers
			 */
			this.onResponse = function(data, headers) {
				onResponse(me, data, headers);
			};

			// watch for param changes.
			$rootScope.$watchCollection(function() {
				return me.params;
			}, function(n, o) {
				if (n.limit !== o.limit) {
					onUpdateLimit(me);
				}
			});
		};

		/**
		 * Force refresh (fetch new data from the server)
		 */
		uNgGrid.prototype.refresh = function() {
			var service = this;
			this.loading = true;
			return this.resource.query(transformParams(this, this.params), this.onResponse).$promise.then(function() {
				if (service.pagination) {
					service.pagination.refresh();
				}
				service.loading = false;
			}, function() {
				service.loading = false;
			});
		};

		return uNgGrid;
	}];
});

angular.module("mingos.uNgGrid").provider("uNgGridPagination", function() {
	var scrollingStyles = {
		/**
		 * Show all pages
		 * @param   {Array}  pages
		 * @returns {Array}
		 */
		all: function(pages) {
			return pages;
		},

		/**
		 * Current page always in the middle; always show same umber of pages
		 * @param   {Array}  pages
		 * @param   {Number} currentPage
		 * @param   {Number} pageRange
		 * @returns {Array}
		 */
		sliding: function(pages, currentPage, pageRange) {
			var pageCount = pages.length,
				lowerBound,
				upperBound;

			if (pageRange > pageCount) {
				pageRange = pageCount;
			}

			var delta = Math.ceil(pageRange / 2);

			if (currentPage - delta > pageCount - pageRange) {
				lowerBound = pageCount - pageRange + 1;
				upperBound = pageCount;
			} else {
				if (currentPage - delta < 0) {
					delta = currentPage;
				}

				var offset     = currentPage - delta;
				lowerBound = offset + 1;
				upperBound = offset + pageRange;
			}

			return pages.slice(pages.indexOf(lowerBound), pages.indexOf(upperBound) + 1);
		},

		/**
		 * Segmented pagination
		 * @param   {Array}  pages
		 * @param   {Number} currentPage
		 * @param   {Number} pageRange
		 * @returns {Array}
		 */
		jumping: function(pages, currentPage, pageRange) {
			var delta = currentPage % pageRange;

			if (0 === delta) {
				delta = pageRange;
			}

			var offset     = currentPage - delta;
			var lowerBound = offset + 1;
			var upperBound = Math.min(pages.length, offset + pageRange);

			return pages.slice(pages.indexOf(lowerBound), pages.indexOf(upperBound) + 1);
		},

		/**
		 * Variable number of pages in paginator, depending on where the current page is
		 * @param   {Array}  pages
		 * @param   {Number} currentPage
		 * @param   {Number} pageRange
		 * @returns {Array}
		 */
		elastic: function(pages, currentPage, pageRange) {
			var originalPageRange = pageRange;
			pageRange = pageRange * 2 - 1;

			if (originalPageRange + currentPage - 1 < pageRange) {
				pageRange = originalPageRange + currentPage - 1;
			} else if (originalPageRange + currentPage - 1 > pages.length) {
				pageRange = originalPageRange + pages.length - currentPage;
			}

			return scrollingStyles.sliding(pages, currentPage, pageRange);
		}
	};

	/**
	 * Default scrolling style
	 * @type {String}
	 */
	var scrollingStyle = "sliding";

	/**
	 * Default page range
	 * @type {Number}
	 */
	var pageRange = 5;

	return {
		setScrollingStyle: function(name) {
			if (name in scrollingStyles) {
				scrollingStyle = name;
			} else {
				throw "Invalid scrolling style name: " + name;
			}
			return this;
		},
		setPageRange: function(range) {
			pageRange = parseInt(range, 10);
			return this;
		},
		$get: function() {
			var uNgGridPagination = function() {
				this.grid = null;
				this.pages = [];
				this.currentPage = 1;
				this.firstPage = 1;
				this.lastPage = 1;
				this.nextPage = 1;
				this.previousPage = 1;
				this.pageRange = pageRange;
				this.scrollingStyle = scrollingStyle;
			};

			/**
			 * Set the current page number (1-based)
			 * @param {Number} page
			 */
			uNgGridPagination.prototype.setPage = function(page) {
				if (this.grid) {
					this.grid.params.offset = (page - 1) * this.grid.params.limit;
				}
			};

			/**
			 * Refresh the pages range
			 */
			uNgGridPagination.prototype.refresh = function() {
				this.pages.length = 0;

				if (this.grid) {
					var i = 0;
					do {
						this.pages.push(this.pages.length + 1);
						if (i >= this.grid.params.offset && i < this.grid.params.offset + this.grid.params.limit) {
							this.currentPage = this.pages.length;
						}
						i += this.grid.params.limit;
					} while (i < this.grid.total);

					this.firstPage = 1;
					this.lastPage = this.pages.length;
					this.previousPage = Math.max(this.firstPage, this.currentPage - 1);
					this.nextPage = Math.min(this.lastPage, this.currentPage + 1);

					this.pages = scrollingStyles[this.scrollingStyle](this.pages, this.currentPage, this.pageRange);
				}
			};

			return uNgGridPagination;
		}
	};
});

angular.module("mingos.uNgGrid").provider("uNgGridSorter", function() {
	var classes = {
		"default": {
			asc: "asc",
			desc: "desc",
			none: "none"
		}
	};
	return {
		/**
		 * Set a default class
		 * @param   {String} type  Type of sorter (alphabetic, numeric, default...). It's possible to define new types.
		 *                         Omitting this parametre will set the value in the "default" type. In order to use
		 *                         new types with the uNgSorter directive, the classes in the new type need to be
		 *                         created here.
		 * @param   {String} key   Key name to set (asc, desc or none)
		 * @param   {String} value Class name for the chosen type & key
		 * @returns {self}
		 */
		setClass: function(type, key, value) {
			if (undefined === value) {
				value = key;
				key = type;
				type = "default";
			}
			classes[type] = classes[type] || {};
			classes[type][key] = value;
			return this;
		},
		$get: function() {
			return {
				get: function(type, key) {
					if (undefined === key) {
						key = type;
						type = "default";
					}

					return classes[type][key] || classes["default"][key];
				}
			};
		}
	};
});
