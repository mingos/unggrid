describe("uNgGrid", function() {
	beforeEach(module("mingos.uNgGrid"));
	beforeEach(module("ngResource"));

	var service, resource, http, grid;
	beforeEach(inject(function(uNgGrid, $resource, $httpBackend) {
		service = uNgGrid;
		resource = $resource;
		http = $httpBackend;
	}));

	afterEach(function() {
		http.verifyNoOutstandingExpectation();
		http.verifyNoOutstandingRequest();
	});

	var url = "http://example.com/lumberjack";
	var response = [
		"The Larch!",
		"The Pine!",
		"The Giant Redwood tree!",
		"The Sequoia!",
		"The Little Whopping Rule Tree!"
	];

	beforeEach(function() {
		grid = new service(resource(url));
	});

	it("sends correct requests", function() {
		http.expectGET(url + "?limit=10&offset=0").respond(response);
		grid.refresh();
		http.flush();

		http.expectGET(url + "?foo=bar&limit=20&offset=5").respond(response);
		grid.params.limit = 20;
		grid.params.offset = 5;
		grid.params.foo = "bar";
		grid.refresh();
		http.flush();
	});

	it("stores fetched data", function() {
		expect(grid.data).toEqual([]);

		http.expectGET(url + "?limit=10&offset=0").respond(response);
		grid.refresh();
		http.flush();

		expect(grid.data.length).toBe(response.length);
		for (var i = 0; i < grid.data.length; ++i) { // for some reason, toEqual() gives false o_O
			expect(grid.data[i]).toBe(response[i]);
		}
	});
});
