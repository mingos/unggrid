describe("uNgGridPagination", function() {
	beforeEach(module("mingos.uNgGrid"));

	var service, grid;
	beforeEach(inject(function(uNgGridPagination) {
		service = uNgGridPagination;
		grid = {
			params: {
				limit: 10,
				offset: 0
			},
			total: 100
		};
	}));

	var paginator;
	beforeEach(function() {
		paginator = new service();
		paginator.grid = grid;
	});

	it("changes pages", function() {
		paginator.setPage(6);
		expect(grid.params.offset).toBe(50);

		paginator.setPage(2);
		expect(grid.params.offset).toBe(10);
	});


	it("works correctly with string limit", function() {
		grid.params.limit = "10";
		paginator.refresh();

		expect(paginator.lastPage).toBe(10);
	});

	describe("Scrolling styles", function() {
		it("calculates sliding (default) style", function() {
			paginator.refresh();
			expect(paginator.pages).toEqual([1, 2, 3, 4, 5]);
			expect(paginator.lastPage).toBe(10);
			expect(paginator.currentPage).toBe(1);

			grid.params.offset = 50;
			paginator.refresh();
			expect(paginator.pages).toEqual([4, 5, 6, 7, 8]);
			expect(paginator.currentPage).toBe(6);

			paginator.pageRange = 3;
			paginator.refresh();
			expect(paginator.pages).toEqual([5, 6, 7]);
			expect(paginator.currentPage).toBe(6);
		});

		it("calculates jumping style", function() {
			paginator.scrollingStyle = "jumping";

			paginator.refresh();
			expect(paginator.pages).toEqual([1, 2, 3, 4, 5]);
			expect(paginator.lastPage).toBe(10);
			expect(paginator.currentPage).toBe(1);

			grid.params.offset = 50;
			paginator.refresh();
			expect(paginator.pages).toEqual([6, 7, 8, 9, 10]);
			expect(paginator.currentPage).toBe(6);

			paginator.pageRange = 3;
			paginator.refresh();
			expect(paginator.pages).toEqual([4, 5, 6]);
			expect(paginator.currentPage).toBe(6);
		});

		it("calculates elastic style", function() {
			paginator.scrollingStyle = "elastic";

			paginator.refresh();
			expect(paginator.pages).toEqual([1, 2, 3, 4, 5]);
			expect(paginator.lastPage).toBe(10);
			expect(paginator.currentPage).toBe(1);

			grid.params.offset = 50;
			paginator.refresh();
			expect(paginator.pages).toEqual([2, 3, 4, 5, 6, 7, 8, 9, 10]);
			expect(paginator.currentPage).toBe(6);

			paginator.pageRange = 3;
			paginator.refresh();
			expect(paginator.pages).toEqual([4, 5, 6, 7, 8]);
			expect(paginator.currentPage).toBe(6);
		});

		it("shows all", function() {
			paginator.scrollingStyle = "all";

			paginator.refresh();
			expect(paginator.pages).toEqual([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
			expect(paginator.lastPage).toBe(10);
			expect(paginator.currentPage).toBe(1);

			grid.params.offset = 50;
			paginator.refresh();
			expect(paginator.pages).toEqual([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
			expect(paginator.currentPage).toBe(6);

			paginator.pageRange = 3;
			paginator.refresh();
			expect(paginator.pages).toEqual([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
			expect(paginator.currentPage).toBe(6);
		});
	});
});
