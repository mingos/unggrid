describe("sorter servicce", function() {
	var provider;
	beforeEach(function() {
		var fake = angular.module("fake", function() {});
		fake.config(function(uNgGridSorterProvider) {
			provider = uNgGridSorterProvider;
		});

		module("mingos.uNgGrid", "fake");
		inject(function() {});
	});

	it("gets default values", function() {
		var service = provider.$get();

		expect(service.get("asc")).toBe("asc");
		expect(service.get("desc")).toBe("desc");
		expect(service.get("none")).toBe("none");
	});

	it("sets and gets custom values", function() {
		provider.setClass("asc", "testAsc");
		provider.setClass("foo", "desc", "testDesc");
		provider.setClass("bar", "none", "testNone");

		var service = provider.$get();

		expect(service.get("default", "asc")).toBe("testAsc");
		expect(service.get("foo", "desc")).toBe("testDesc");
		expect(service.get("bar", "none")).toBe("testNone");
		expect(service.get("bar", "asc")).toBe("testAsc"); // falls back to "default"
		expect(service.get("bar", "desc")).toBe("desc"); // falls back to "default"
	});
});
