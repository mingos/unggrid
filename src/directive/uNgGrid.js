angular.module("mingos.uNgGrid").directive("uNgGrid", function() {
	return {
		restrict: "A",
		scope: {
			service: "=uNgGrid"
		},
		controller: function($scope) {
			this.service = $scope.service;
		}
	};
});
