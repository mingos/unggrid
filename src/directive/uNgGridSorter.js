angular.module("mingos.uNgGrid").directive("uNgGridSorter", function(
	uNgGridSorter
) {
	return {
		restrict: "AE",
		replace: true,
		require: "^uNgGrid",
		template:
			"<span class=\"u-ng-grid-sorter\" ng-click=\"sort()\">" +
				"<span class=\"{{ sortingClass }}\"></span>" +
			"</span>",
		scope: {
			field: "@"
		},
		link: function(scope, element, attributes, uNgGrid) {
			var type = attributes.type || "default";
			var classes = {
				asc: attributes.classAsc || uNgGridSorter.get(type, "asc"),
				desc: attributes.classDesc || uNgGridSorter.get(type, "desc"),
				none: attributes.classNone || uNgGridSorter.get(type, "none")
			};
			scope.sortingClass = classes.none;
			scope.service = uNgGrid.service;

			scope.$watchCollection("service.params", function() {
				if (scope.field === scope.service.params.orderfield) {
					if (-1 === scope.service.params.orderdirection) {
						scope.sortingClass = classes.desc;
					} else {
						scope.sortingClass = classes.asc;
					}
				} else {
					scope.sortingClass = classes.none;
				}
			});

			scope.sort = function() {
				if (scope.field === scope.service.params.orderfield) {
					scope.service.params.orderdirection = scope.service.params.orderdirection ?
					scope.service.params.orderdirection * -1 :
						1;
				} else {
					scope.service.params.orderdirection = 1;
				}

				scope.service.params.orderfield = scope.field;
			};
		}
	};
});
