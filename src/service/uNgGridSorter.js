angular.module("mingos.uNgGrid").provider("uNgGridSorter", function() {
	var classes = {
		"default": {
			asc: "asc",
			desc: "desc",
			none: "none"
		}
	};
	return {
		/**
		 * Set a default class
		 * @param   {String} type  Type of sorter (alphabetic, numeric, default...). It's possible to define new types.
		 *                         Omitting this parametre will set the value in the "default" type. In order to use
		 *                         new types with the uNgSorter directive, the classes in the new type need to be
		 *                         created here.
		 * @param   {String} key   Key name to set (asc, desc or none)
		 * @param   {String} value Class name for the chosen type & key
		 * @returns {self}
		 */
		setClass: function(type, key, value) {
			if (undefined === value) {
				value = key;
				key = type;
				type = "default";
			}
			classes[type] = classes[type] || {};
			classes[type][key] = value;
			return this;
		},
		$get: function() {
			return {
				get: function(type, key) {
					if (undefined === key) {
						key = type;
						type = "default";
					}

					return classes[type][key] || classes["default"][key];
				}
			};
		}
	};
});
