angular.module("mingos.uNgGrid").provider("uNgGrid", function() {
	/**
	 * Default server response callback
	 * @param {uNgGrid}  service
	 * @param {Array}    data
	 * @param {Function} headers
	 */
	var onResponse = function(service, data, headers) {
		service.data = data;
		service.total = parseInt(headers("Total-Items"), 10);
	};

	/**
	 * Default callback launched when the limit is updated
	 * @param {uNgGrid} service
	 */
	var onUpdateLimit = function(service) {
		service.params.offset = 0;
	};

	/**
	 * Default parametres transformation for sending along with the request
	 * @param   {uNgGrid} service
	 * @param   {Object}  params
	 * @returns {Object}
	 */
	var transformParams = function(service, params) {
		return params;
	};

	/**
	 * Set the default onResponse callback
	 * @param   {Function} cbk
	 * @returns {self}
	 */
	this.setOnResponse = function(cbk) {
		onResponse = cbk;
		return this;
	};

	/**
	 * Set the default onUpdateLimit callback
	 * @param   {Function} cbk
	 * @returns {self}
	 */
	this.setOnUpdateLimit = function(cbk) {
		onUpdateLimit = cbk;
		return this;
	};

	/**
	 * Set a custom parametre transformation function
	 * @param   {Function} cbk
	 * @returns {self}
	 */
	this.setTransformParams = function(cbk) {
		transformParams = cbk;
		return this;
	};

	/**
	 * Factory
	 * @returns {Function}
	 */
	this.$get = ["$rootScope", function($rootScope) {
		var uNgGrid = function(resource, pagination) {
			var me = this;

			/**
			 * A resource class (not instance)
			 */
			this.resource = resource;

			/**
			 * Parametres that will be sent with each request
			 */
			this.params = {
				limit: 10,
				offset: 0
			};

			/**
			 * Currently available data
			 */
			this.data = [];

			/**
			 * Total number of items that would match the current query without offset + limit
			 * @type {Number}
			 */
			this.total = 0;

			/**
			 * Flag indicating whether data is currently being loaded
			 * @type {Boolean}
			 */
			this.loading = false;

			/**
			 * Grid's pagination service
			 * @type {uNgGridPagination}
			 */
			this.pagination = pagination;
			if (this.pagination) {
				this.pagination.grid = this;
			}

			/**
			 * Callback used after the server responds with some data
			 * @param {Array}    data
			 * @param {Function} headers
			 */
			this.onResponse = function(data, headers) {
				onResponse(me, data, headers);
			};

			// watch for param changes.
			$rootScope.$watchCollection(function() {
				return me.params;
			}, function(n, o) {
				if (n.limit !== o.limit) {
					onUpdateLimit(me);
				}
			});
		};

		/**
		 * Force refresh (fetch new data from the server)
		 */
		uNgGrid.prototype.refresh = function() {
			var service = this;
			this.loading = true;
			return this.resource.query(transformParams(this, this.params), this.onResponse).$promise.then(function() {
				if (service.pagination) {
					service.pagination.refresh();
				}
				service.loading = false;
			}, function() {
				service.loading = false;
			});
		};

		return uNgGrid;
	}];
});
