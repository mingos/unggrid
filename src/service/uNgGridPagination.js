angular.module("mingos.uNgGrid").provider("uNgGridPagination", function() {
	var scrollingStyles = {
		/**
		 * Show all pages
		 * @param   {Array}  pages
		 * @returns {Array}
		 */
		all: function(pages) {
			return pages;
		},

		/**
		 * Current page always in the middle; always show same umber of pages
		 * @param   {Array}  pages
		 * @param   {Number} currentPage
		 * @param   {Number} pageRange
		 * @returns {Array}
		 */
		sliding: function(pages, currentPage, pageRange) {
			var pageCount = pages.length,
				lowerBound,
				upperBound;

			if (pageRange > pageCount) {
				pageRange = pageCount;
			}

			var delta = Math.ceil(pageRange / 2);

			if (currentPage - delta > pageCount - pageRange) {
				lowerBound = pageCount - pageRange + 1;
				upperBound = pageCount;
			} else {
				if (currentPage - delta < 0) {
					delta = currentPage;
				}

				var offset     = currentPage - delta;
				lowerBound = offset + 1;
				upperBound = offset + pageRange;
			}

			return pages.slice(pages.indexOf(lowerBound), pages.indexOf(upperBound) + 1);
		},

		/**
		 * Segmented pagination
		 * @param   {Array}  pages
		 * @param   {Number} currentPage
		 * @param   {Number} pageRange
		 * @returns {Array}
		 */
		jumping: function(pages, currentPage, pageRange) {
			var delta = currentPage % pageRange;

			if (0 === delta) {
				delta = pageRange;
			}

			var offset     = currentPage - delta;
			var lowerBound = offset + 1;
			var upperBound = Math.min(pages.length, offset + pageRange);

			return pages.slice(pages.indexOf(lowerBound), pages.indexOf(upperBound) + 1);
		},

		/**
		 * Variable number of pages in paginator, depending on where the current page is
		 * @param   {Array}  pages
		 * @param   {Number} currentPage
		 * @param   {Number} pageRange
		 * @returns {Array}
		 */
		elastic: function(pages, currentPage, pageRange) {
			var originalPageRange = pageRange;
			pageRange = pageRange * 2 - 1;

			if (originalPageRange + currentPage - 1 < pageRange) {
				pageRange = originalPageRange + currentPage - 1;
			} else if (originalPageRange + currentPage - 1 > pages.length) {
				pageRange = originalPageRange + pages.length - currentPage;
			}

			return scrollingStyles.sliding(pages, currentPage, pageRange);
		}
	};

	/**
	 * Default scrolling style
	 * @type {String}
	 */
	var scrollingStyle = "sliding";

	/**
	 * Default page range
	 * @type {Number}
	 */
	var pageRange = 5;

	return {
		setScrollingStyle: function(name) {
			if (name in scrollingStyles) {
				scrollingStyle = name;
			} else {
				throw "Invalid scrolling style name: " + name;
			}
			return this;
		},
		setPageRange: function(range) {
			pageRange = parseInt(range, 10);
			return this;
		},
		$get: function() {
			var uNgGridPagination = function() {
				this.grid = null;
				this.pages = [];
				this.currentPage = 1;
				this.firstPage = 1;
				this.lastPage = 1;
				this.nextPage = 1;
				this.previousPage = 1;
				this.pageRange = pageRange;
				this.scrollingStyle = scrollingStyle;
			};

			/**
			 * Set the current page number (1-based)
			 * @param {Number} page
			 */
			uNgGridPagination.prototype.setPage = function(page) {
				if (this.grid) {
					this.grid.params.offset = (page - 1) * this.grid.params.limit;
				}
			};

			/**
			 * Refresh the pages range
			 */
			uNgGridPagination.prototype.refresh = function() {
				this.pages.length = 0;
				var limit = parseInt(this.grid.params.limit, 10);

				if (this.grid) {
					var i = 0;
					do {
						this.pages.push(this.pages.length + 1);
						if (i >= this.grid.params.offset && i < this.grid.params.offset + limit) {
							this.currentPage = this.pages.length;
						}
						i += limit;
					} while (i < this.grid.total);

					this.firstPage = 1;
					this.lastPage = this.pages.length;
					this.previousPage = Math.max(this.firstPage, this.currentPage - 1);
					this.nextPage = Math.min(this.lastPage, this.currentPage + 1);

					this.pages = scrollingStyles[this.scrollingStyle](this.pages, this.currentPage, this.pageRange);
				}
			};

			return uNgGridPagination;
		}
	};
});
