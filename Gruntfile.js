module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON("package.json"),
		copyright: "/*! uNgGrid <%= pkg.version %>\n" +
			" *  Copyright (c) 2015 Dominik Marczuk\n" +
			" *  Licence: BSD-2-Clause: http://opensource.org/licenses/BSD-2-Clause\n" +
			" */\n",
		jshint: {
			test: {
				options: {
					browser: true,
					camelcase: true,
					curly: true,
					eqeqeq: true,
					es3: true,
					forin: true,
					freeze: true,
					immed: true,
					indent: 4,
					latedef: true,
					maxlen: 120,
					newcap: true,
					noarg: true,
					noempty: true,
					nomen: true,
					nonbsp: false,
					nonew: true,
					plusplus: false,
					quotmark: true,
					undef: true,
					strict: false,
					trailing: true,
					unused: true,
					globals: {
						angular: true
					}
				},
				files: {
					src: ["src/**/*.js"]
				}
			}
		},
		karma: {
			unit: {
				configFile: "karma.conf.js"
			}
		},
		connect: {
			demo: {
				options: {
					index: "demo/index.html",
					base: ".",
					keepalive: true,
					middleware: function(connect, options, middlewares) {
						var myMiddlewares = require("./demo/connect");
						for (var i = 0; i < myMiddlewares.length; ++i) {
							middlewares.unshift(myMiddlewares[i]);
						}
						return middlewares;
					}
				}
			}
		},
		concat: {
			options: {
				banner: "<%= copyright %>"
			},
			dist: {
				files: {
					"dist/uNgGrid.js": [
						"src/**/*"
					]
				}
			}
		},
		uglify: {
			options: {
				banner: "<%= copyright %>"
			},
			dist: {
				files: {
					"dist/uNgGrid.min.js": [
						"dist/uNgGrid.js"
					]
				}
			}
		}
	});

	// Load tasks
	grunt.loadNpmTasks("grunt-contrib-concat");
	grunt.loadNpmTasks("grunt-contrib-connect");
	grunt.loadNpmTasks("grunt-contrib-jshint");
	grunt.loadNpmTasks("grunt-contrib-uglify");
	grunt.loadNpmTasks("grunt-karma");

	// Custom tasks
	grunt.registerTask("default", []);
	grunt.registerTask("demo", ["connect:demo"]);
	grunt.registerTask("dist", ["concat:dist", "uglify:dist"]);
	grunt.registerTask("test", ["jshint:test", "karma:unit"]);
};
