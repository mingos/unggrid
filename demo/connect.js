var fs = require("fs");
var qs = require("querystring");

module.exports = [
	// redirect from base URL to the demo file
	function(req, res, next) {
		if (req.url !== "/") {
			return next();
		}

		res.statusCode = 302;
		res.setHeader("Location", "/demo/index.html");
		res.setHeader("Content-Length", "0");
		res.end();
	},

	// serve the data
	function(req, res, next) {
		if (!req.url.match(/^\/people/)) {
			return next();
		}

		var people = JSON.parse(fs.readFileSync("./demo/data.json"));
		var splitUrl = req.url.split("?");
		var query = (2 === splitUrl.length ? qs.parse(splitUrl[1]) : {});

		// order
		if (query.orderfield) {
			query.orderdirection = parseInt(query.orderdirection || 1);

			people.sort(function(a, b) {
				if ("number" === typeof a[query.orderfield]) {
					return a[query.orderfield] - b[query.orderfield] * query.orderdirection;
				} else {
					return (a[query.orderfield] > b[query.orderfield] ? query.orderdirection : -query.orderdirection);
				}
			});
		}

		// filter
		if (query.filter) {
			people = people.filter(function(a) {
				return a.last.toLowerCase().indexOf(query.filter.toLowerCase()) > -1;
			});
		}

		var total = people.length;

		// offset
		if (query.offset) {
			people.splice(0, parseInt(query.offset, 10));
		}

		// limit
		if (query.limit) {
			query.limit = parseInt(query.limit, 10);
			if (people.length > query.limit) {
				people.splice(query.limit);
			}
		}

		res.setHeader("Access-Control-Allow-Origin", "*");
		res.setHeader("Access-Control-Expose-Headers", "Total-Items");
		res.setHeader("Total-Items", total.toString());
		res.setHeader("Content-Type", "application/json");

		// send response
		var response = JSON.stringify(people);
		if (query.timeout) {
			setTimeout(function() {
				res.end(response);
			}, parseInt(query.timeout, 10));
		} else {
			res.end(response);
		}
	}
];
