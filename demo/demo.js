var app = angular.module("demo", ["ngResource", "mingos.uNgGrid"]).config(function(
	uNgGridSorterProvider,
	uNgGridProvider
) {
	// tell the sorter service what classes to apply on the sorting buttons
	uNgGridSorterProvider
		.setClass("default", "asc", "glyphicon glyphicon-sort-by-attributes")
		.setClass("default", "desc", "glyphicon glyphicon-sort-by-attributes-alt")
		.setClass("default", "none", "glyphicon glyphicon-sort")
		.setClass("alphabetic", "asc", "glyphicon glyphicon-sort-by-alphabet")
		.setClass("alphabetic", "desc", "glyphicon glyphicon-sort-by-alphabet-alt")
		.setClass("numeric", "asc", "glyphicon glyphicon-sort-by-order")
		.setClass("numeric", "desc", "glyphicon glyphicon-sort-by-order-alt");

	uNgGridProvider
		.setTransformParams(function(service, params) {
			var ret = angular.extend({ }, params);
			if (service.timeout) {
				ret.timeout = 1000;
			}
			return ret;
		});
});

app.controller("DemoCtrl", function(
	$scope,
	$resource,
	uNgGridPagination,
	uNgGrid
) {
	var resource = $resource("/people");

	$scope.people = new uNgGrid(resource, new uNgGridPagination());
	$scope.$watchCollection("people.params", function() {
		$scope.people.refresh();
	});
});
