# uNgGrid

uNgGrid is an Angular.js module providing support for flexible, paginated grids that make it easy to fetch data from the
server and perform typical tasks such as filtering, ordering and pagination.

The module provides services for dealing with grid-related tasks as well as directives that make use of the services.

## Installation

### In a project

You can add uNgGrid to your project using Bower. Just add the following dependency:

    "dependencies": {
        "mingos-unggrid": "https://bitbucket.org/mingos/unggrid.git"
    }

### Standalone

You can also check out uNgGrid as a standalone project, e.g. for code evaluation or in order to see the demo. Once the
project is cloned from its repo, install all its dependencies:

    npm install && bower install

## Demo

In order to launch a demo application and see uNgGrid in action, run the `demo` task:

    grunt demo

Now you can navigate to `http://localhost:8000` to see the grid in action.

## Tests

In order to run the tests, use the `test` command:

    grunt test
